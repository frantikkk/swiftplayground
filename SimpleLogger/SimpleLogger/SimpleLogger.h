//
//  SimpleLogger.h
//  SimpleLogger
//
//  Created by Yury Shalin on 7/9/15.
//  Copyright (c) 2015 Yury Shalin. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SimpleLogger.
FOUNDATION_EXPORT double SimpleLoggerVersionNumber;

//! Project version string for SimpleLogger.
FOUNDATION_EXPORT const unsigned char SimpleLoggerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SimpleLogger/PublicHeader.h>


