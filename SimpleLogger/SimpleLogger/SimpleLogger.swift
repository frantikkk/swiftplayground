//
//  SimpleLogger.swift
//  SimpleLogger
//
//  Created by Yury Shalin on 7/9/15.
//  Copyright (c) 2015 Yury Shalin. All rights reserved.
//

import Foundation


public class SimpleLogger {
    
    public init () {
        
    }

    public func log(logString: String) {
        print("\(logString)\n")
    }
}