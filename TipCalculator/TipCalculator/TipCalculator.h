//
//  TipCalculator.h
//  TipCalculator
//
//  Created by Yury Shalin on 7/8/15.
//  Copyright (c) 2015 Yury Shalin. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TipCalculator.
FOUNDATION_EXPORT double TipCalculatorVersionNumber;

//! Project version string for TipCalculator.
FOUNDATION_EXPORT const unsigned char TipCalculatorVersionString[];



// In this header, you should import all the public headers of your framework using statements like #import <TipCalculator/PublicHeader.h>
