//
//  ViewController.swift
//  TipCalculator
//
//  Created by Main Account on 12/18/14.
//  Copyright (c) 2014 Razeware LLC. All rights reserved.
//

import UIKit
import SimpleLogger

public protocol TipCalculatorViewControllerDelegate
{
    func closeTapped(sender: AnyObject)
}

 
public class TipCalculatorViewController: UIViewController {
    
    let logger = SimpleLogger()

    @IBOutlet var totalTextField : UITextField!
    @IBOutlet var taxPctSlider : UISlider!
    @IBOutlet var taxPctLabel : UILabel!
    @IBOutlet var resultsTextView : UITextView!
    
    public var delegate:TipCalculatorViewControllerDelegate?
    
    let tipCalc = TipCalculatorModel(total: 33.25, taxPct: 0.06)
  
    func refreshUI() {
        totalTextField.text = String(format: "%0.2f", tipCalc.total)
        taxPctSlider.value = Float(tipCalc.taxPct) * 100.0
        taxPctLabel.text = "Tax Percentage (\(Int(taxPctSlider.value))%)"
        resultsTextView.text = ""
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        logger.log("Tip Calculator loaded")
        refreshUI()
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

// MARK: Actions

    @IBAction func calculateTapped(sender : AnyObject) {
        tipCalc.total = Double((totalTextField.text as NSString).doubleValue)
        let possibleTips = tipCalc.returnPossibleTips()
        var results = ""
        for (tipPct, tipValue) in possibleTips {
            results += "\(tipPct)%: \(tipValue)\n"
        }
        resultsTextView.text = results
    }

    @IBAction func taxPercentageChanged(sender : AnyObject) {
        tipCalc.taxPct = Double(taxPctSlider.value) / 100.0
        refreshUI()
    }
  
    @IBAction func viewTapped(sender : AnyObject) {
        totalTextField.resignFirstResponder()
    }
    
    @IBAction func didTapClose(sender: AnyObject) {
        delegate?.closeTapped(self)
    }
}

