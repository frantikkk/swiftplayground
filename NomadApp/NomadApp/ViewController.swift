//
//  ViewController.swift
//  NomadApp
//
//  Created by Yury Shalin on 7/9/15.
//  Copyright (c) 2015 Yury Shalin. All rights reserved.
//

import UIKit
import TipCalculator
import SimpleLogger

class ViewController: UIViewController, TipCalculatorViewControllerDelegate {
    
    let logger = SimpleLogger()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func didTapTipCalculator(sender: AnyObject) {
        logger.log("Tip Calculator button tapped")
        
        let frameworkBundle: NSBundle = NSBundle(forClass:TipCalculatorViewController.self);
        let storyboard = UIStoryboard(name: "Main", bundle:frameworkBundle)
        let vc :TipCalculatorViewController  = storyboard.instantiateViewControllerWithIdentifier("TipCalculatorViewController") as! TipCalculatorViewController
        vc.delegate = self
        self.presentViewController(vc, animated: true, completion:nil)
    }
    
// MARK: TipCalculatorViewControllerDelegate implementation
    
    func closeTapped(sender: AnyObject) {
        logger.log("Tip Calculator is about to be closed")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

